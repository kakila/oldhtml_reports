<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>GP model of sensor measurements</title>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']] },
  TeX: { equationNumbers: { autoNumber: 'all' } }
});
</script>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

<style>
body > * {
  max-width: 42em;
}
body {
  font-family: "Roboto Condensed", sans-serif;
  padding-left: 7.5em;
  padding-right: 7.5em;
}
pre, code {
  max-width: 50em;
  font-family: monospace;
}
pre.oct-code {
  border: 1px solid Grey;
  padding: 5px;
}
pre.oct-code-output {
  margin-left: 2em;
}
span.comment {
  color: ForestGreen;
}
span.keyword {
  color: Blue;
}
span.string {
  color: DarkOrchid;
}
footer {
  margin-top: 2em;
  font-size: 80%;
}
a, a:visited {
  color: Blue;
}
h2 {
  font-family: "Roboto Condensed", serif;
  margin-top: 1.5em;
}
h2 a, h2 a:visited {
  color: Black;
}
</style>

</head>
<body>
<h1>GP model of sensor measurements</h1>

<p>Copyright (C) 2018 Juan Pablo Carbajal</p>

<p>This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.</p>

<p>This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>

<p>Author: Juan Pablo Carbajal <a href="ajuanpi+dev@gmail.com">ajuanpi+dev@gmail.com</a>
Created: 2018-07-25</p>

<h2>Contents</h2>
<ul>
<li><a href="#node1">Dependencies</a></li>
<li><a href="#node2">Data and model description</a></li>
<li><a href="#node3">Gaussian process (GP) model</a></li>
</ul>
<h2><a id="node1">Dependencies</a></h2>
<p>Load all dependencies of this script</p>

<pre class="oct-code">pkg load signal      <span class="comment"># for autocorrelation</span>
pkg load statistics  <span class="comment"># for sampling multivariate normal distribution</span>
</pre>

<p>To install <i>gpml</i> in Octave 4.4.0 or later run</p>

<p><i>pkg install https://bitbucket.org/KaKiLa/gpml/downloads/gpml-4.2.0.tar.gz</i></p>

<pre class="oct-code">pkg load gpml       <span class="comment"># for gp models</span>
</pre>
<h2><a id="node2">Data and model description</a></h2>
<p>The next plots show the raw data and a simple linear regression
The results of the linear regression will be used to initialize the GP model
in the subsequent sections.</p>

<pre class="oct-code">data = dlmread (<span class="string">'data1.csv'</span>, <span class="string">';'</span>, 1, 1);
t1   = data(:,1);
z1   = data(:,2);
N1   = length (z1);
pz1  = polyfit (t1, z1, 1);
z1_  = polyval(pz1, t1);

data = dlmread (<span class="string">'data2.csv'</span>, <span class="string">';'</span>, 1, 1);
t2   = data(:,1);
z2   = data(:,2);
N2   = length (z2);
pz2  = polyfit (t2, z2, 1);
z2_  = polyval(pz2, t2);</pre>

<p>Plots of the raw data and the linear fit</p>

<pre class="oct-code">figure (1), clf
  hold on
  h = plot (t1, z1, <span class="string">'^'</span>, t2, z2, <span class="string">'v'</span>);
  plot (t1, z1_, <span class="string">'-'</span>, <span class="string">'color'</span>, get (h(1), <span class="string">'color'</span>))
  plot (t2, z2_, <span class="string">'-'</span>, <span class="string">'color'</span>, get (h(2), <span class="string">'color'</span>))
  axis tight
  hold off
  xlabel(<span class="string">'Time [d]'</span>)
  ylabel(<span class="string">'Measurement [mV]'</span>)
  legend(h, {<span class="string">'Run 1'</span>,<span class="string">'Run 2'</span>})</pre>
<img src="s_gpmodel-1.png" alt="s_gpmodel-1.png">
<p>Plots of the residuals, and their autocorrelation functions.</p>

<pre class="oct-code">res1 = z1 - z1_;
res2 = z2 - z2_;
[acorr1 l1] = xcorr (res1, <span class="string">'coeff'</span>);
[acorr2 l2] = xcorr (res2, <span class="string">'coeff'</span>);

figure (2), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, <span class="string">'^;Run 1;'</span>, t2, res2, <span class="string">'v;Run 2;'</span>);
  ylabel(<span class="string">'Residual [mV]'</span>)
  axis tight

  <span class="comment"># Autocorrelation of residuals</span>
  subplot (2, 1, 2)
  hold on
  stem (l1, acorr1, <span class="string">'color'</span>, get (hres(1), <span class="string">'color'</span>), <span class="string">'filled'</span>);
  stem (l2, acorr2, <span class="string">'color'</span>, get (hres(2), <span class="string">'color'</span>), <span class="string">'filled'</span>);
  ylabel(<span class="string">'Autocorrelation coeff.'</span>)
  xlabel(<span class="string">'Lag [d]'</span>)
  axis tight
  hold off</pre>
<img src="s_gpmodel-2.png" alt="s_gpmodel-2.png"><h2><a id="node3">Gaussian process (GP) model</a></h2>
<p>The data generating model is:</p>

<p>$$ \dot{x} = \xi(t) $$
$$ \dot{y} = x(t) $$
$$ z_k  = y(t_k) + \zeta_k$$
it is defined in $t \in [0,\infty)$ with initial conditions
$x(0) = x_o, y(0) = y_o $</p>

<p>The random variables have the following distributions
$$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
$$ \zeta_k \sim N(0,\sigma^2_{\zeta}) $$</p>

<p>This model has the following corresponding  GP.</p>

<p>The state $x(t)$ is white noise,
$$x(t) \sim \mathcal{N}(x_0, W_0)$$
where $W_0$ corresponds to the Wiener process covariance.
This covariance corresponds to <i>covW(i=0)</i> in the gpml package.</p>

<p>The state $y(t)$ is the 1-time integrated Wiener process,
$$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
its covariance $W_1$ corresponds to <i>covW(i=1)</i> in the gpml package.</p>

<p>Finally, the observable $z_k := z(t=t_k)$ is modeled as
$$ z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$</p>

<pre class="oct-code"><span class="comment"># initial values for hyper-parameters</span>
hyp10 = hyp20 = struct (<span class="string">'cov'</span>, [], <span class="string">'mean'</span>, [], <span class="string">'lik'</span>, []);</pre>

<p>Linear mean function for the observable</p>

<pre class="oct-code">meanfunc = {@meanSum, {
                       @meanLinear, @meanConst
                      }
           };
hyp10.mean = pz1;
hyp20.mean = pz2;</pre>

<p>The likelihood hyper-parameter is the intensity of the observational noise
the value is $\log \left(\sigma_\zeta \right)$</p>

<pre class="oct-code">likfunc = @likGauss;
hyp10.lik = hyp20.lik = 0;</pre>

<p>Covariance function for the continuous observable $z(t)$
The value of the hyper-parameter is the logarithm  of the standard deviation
of the 1-time integrated Wiener process.</p>

<pre class="oct-code">covfunc = {@covW, 1};
hyp10.cov = hyp20.cov = 0;
<span class="comment"># The following is equivalent, if we fix the likelihood hyper-parameter to -Inf</span>
<span class="comment">#covfunc = {@covSum, {</span>
<span class="comment">#                     {@covW, 1}, @covNoise</span>
<span class="comment">#                     }</span>
<span class="comment">#           }</span>
<span class="comment">#hyp.cov = [0; 0];</span>

args  = {@infExact, meanfunc, covfunc, likfunc};</pre>

<p>Find maximum likelihood parameters</p>

<p>Note: the evaluation is done with <i>evalc</i> because the <i>minimize</i> function
is too verbose.</p>

<pre class="oct-code"><span class="keyword">if</span> ~exist (<span class="string">'hyp1'</span>, <span class="string">'var'</span>)
  printf (<span class="string">'Optimizing for Run 1 ...\n'</span>); fflush (stdout);
  <span class="comment">#hyp1  = minimize (hyp10, @gp, -1e3, args{:}, t1, z1);</span>
  st = evalc (<span class="string">'hyp1 = minimize (hyp10, @gp, -1e3, args{:}, t1, z1);'</span>);
  printf (<span class="string">'%s'</span>, st(end-48:end)); fflush (stdout);
  nlml1 = gp (hyp1, args{:}, t1, z1);
<span class="keyword">endif</span>

<span class="keyword">if</span> ~exist (<span class="string">'hyp2'</span>, <span class="string">'var'</span>)
  printf (<span class="string">'Optimizing for Run 2 ...\n'</span>); fflush (stdout);
  <span class="comment">#hyp2  = minimize (hyp20, @gp, -1e3, args{:}, t2, z2);</span>
  st = evalc (<span class="string">'hyp2 = minimize (hyp20, @gp, -1e3, args{:}, t2, z2);'</span>);
  printf (<span class="string">'%s'</span>, st(end-48:end)); fflush (stdout);
  [nlml2, ~, post] = gp (hyp2, args{:}, t2, z2);
<span class="keyword">endif</span></pre>

<pre class="oct-code-output">Optimizing for Run 1 ...
Function evaluation    237;  Value 8.415618e+01
Optimizing for Run 2 ...
Function evaluation    141;  Value 7.538216e+01
</pre>

<p>Generate predictions in an extended interval</p>

<pre class="oct-code">t         = linspace (0, max([t1(:); t2(:)])*1.25, 100).<span class="string">';</span>
[z1_ dz2] = gp (hyp1, args{:}, t1, z1, t);
dz1_      = 1.96 * sqrt (dz2);
[z2_ dz2] = gp (hyp2, args{:}, t2, z2, t);
dz2_      = 1.96 * sqrt (dz2);</pre>

<p>Plot the GP prediction</p>

<pre class="oct-code">figure (3), clf
  hold on
  hz1 = shadowplot (t, z1_, dz1_);
  hz2 = shadowplot (t, z2_, dz2_);
  h = plot (t1, z1, <span class="string">'^'</span>, t2, z2, <span class="string">'v'</span>);
  clr = get (h(1), <span class="string">'color'</span>);
  set (hz1.line.center, <span class="string">'color'</span>, clr);
  set ([hz1.line.top hz1.line.bottom], <span class="string">'color'</span>, min (clr*0.8, 1),
    <span class="string">'linewidth'</span>, 1);
  set (hz1.patch, <span class="string">'facecolor'</span>, min (clr*3, 1));
  <span class="comment">#set (hz1.patch, 'facecolor', clr, 'facealpha', 0.3); # no alpha in html files</span>
  clr = get (h(2), <span class="string">'color'</span>);
  set (hz2.line.center, <span class="string">'color'</span>, clr);
  set ([hz2.line.top hz2.line.bottom], <span class="string">'color'</span>, min (clr*0.8, 1),
    <span class="string">'linewidth'</span>, 1);
  set (hz2.patch, <span class="string">'facecolor'</span>, min (clr*3, 1));
  <span class="comment">#set (hz2.patch, 'facecolor', clr, 'facealpha', 0.3); # no alpha in html files</span>
  xlabel(<span class="string">'Time [d]'</span>)
  ylabel(<span class="string">'Measurement [mV]'</span>)
  legend(h, {<span class="string">'Run 1'</span>,<span class="string">'Run 2'</span>})
  axis tight
  hold off</pre>
<img src="s_gpmodel-3.png" alt="s_gpmodel-3.png">
<p>The estimation of the quantities of interest</p>

<pre class="oct-code">printf (<span class="string">"Run 1:\n"</span>)
printf (<span class="string">"    x(0) = %.2f mV/day\n"</span>, hyp1.mean(1))
printf (<span class="string">"    y(0) = %.2f mV\n"</span>, hyp1.mean(2))
printf (<span class="string">"    std. dev. y(t) = %.2f mV\n"</span>, exp (hyp1.cov))
printf (<span class="string">"    std. dev. \zeta(t) = %.2f mV\n"</span>, exp (hyp1.lik))
printf (<span class="string">"Run 2:\n"</span>)
printf (<span class="string">"    x(0) = %.2f mV/day\n"</span>, hyp2.mean(1))
printf (<span class="string">"    y(0) = %.2f mV\n"</span>, hyp2.mean(2))
printf (<span class="string">"    std. dev. y(t) = %.2f mV\n"</span>, exp (hyp2.cov))
printf (<span class="string">"    std. dev. \zeta(t) = %.2f mV\n"</span>, exp (hyp2.lik))
fflush (stdout);</pre>

<pre class="oct-code-output">Run 1:
    x(0) = -0.35 mV/day
    y(0) = 3.31 mV
    std. dev. y(t) = 0.01 mV
    std. dev. zeta(t) = 1.19 mV
Run 2:
    x(0) = -0.42 mV/day
    y(0) = 4.10 mV
    std. dev. y(t) = 0.01 mV
    std. dev. zeta(t) = 0.90 mV
</pre>

<p>Plots of the residuals, and their autocorrelation functions.</p>

<pre class="oct-code">res1 = z1 - gp (hyp1, args{:}, t1, z1, t1);
res2 = z2 - gp (hyp2, args{:}, t2, z2, t2);
[acorr1 l1] = xcorr (res1);
[acorr2 l2] = xcorr (res2);

figure (4), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, <span class="string">'^;Run 1;'</span>, t2, res2, <span class="string">'v;Run 2;'</span>);
  ylabel(<span class="string">'Residual [mV]'</span>)
  axis tight

  <span class="comment"># Autocorrelation of residuals</span>
  subplot (2, 1, 2)
  hold on
  stem (l1, acorr1, <span class="string">'color'</span>, get (hres(1), <span class="string">'color'</span>), <span class="string">'filled'</span>);
  stem (l2, acorr2, <span class="string">'color'</span>, get (hres(2), <span class="string">'color'</span>), <span class="string">'filled'</span>);
  xlabel(<span class="string">'Time [d]'</span>)
  axis tight
  hold off</pre>
<img src="s_gpmodel-4.png" alt="s_gpmodel-4.png">
<p>Posterior (or conditioned) covariance matrices</p>

<p>Note: can be computed more efficiently from the structure <i>post</i></p>

<pre class="oct-code"><span class="keyword">function</span> K = posteriorcov (covfunc, hyp, obs, tst)
  Koo     = feval (covfunc{:}, hyp.cov, obs);
  Kto     = feval (covfunc{:}, hyp.cov, tst, obs);
  Ktt     = feval (covfunc{:}, hyp.cov, tst);
  Koo_inv = cholinv (Koo + exp (2 * hyp.lik) * eye (length (obs)));
  K       = Ktt - Kto * Koo_inv * Kto.<span class="string">'; # Schur complement</span>
<span class="keyword">endfunction</span>

K1 = posteriorcov (covfunc, hyp1, t1, t);
K2 = posteriorcov (covfunc, hyp2, t2, t);

<span class="comment"># Sample the posteriors distributions</span>
z1_samp = mvnrnd (z1_, K1, 25).<span class="string">';</span>
z2_samp = mvnrnd (z2_, K2, 25).<span class="string">';</span></pre>

<p>Plot posterior mean, 95% CI, and samples from the posterior distribution</p>

<pre class="oct-code">figure (5), clf
  hold on
  plot (t, [z1_samp z2_samp], <span class="string">'-'</span>, <span class="string">'color'</span>, [0.8 0.8 0.8], <span class="string">'linewidth'</span>, 1);
  hz1 = plot (t, z1_ + [0 1 -1] .* dz1_, <span class="string">'-'</span>, <span class="string">'linewidth'</span>, 1);
  hz2 = plot (t, z2_ + [0 1 -1] .* dz2_, <span class="string">'-'</span>, <span class="string">'linewidth'</span>, 1);
  h = plot (t1, z1, <span class="string">'^'</span>, t2, z2, <span class="string">'v'</span>);
  clr = get (h(1), <span class="string">'color'</span>);
  set (hz1(1), <span class="string">'color'</span>, clr);
  set (hz1(2:3), <span class="string">'color'</span>, clr * 0.8);
  set (hz1(2), <span class="string">'color'</span>, get (h(1), <span class="string">'color'</span>));
  clr = get (h(2), <span class="string">'color'</span>);
  set (hz2(1), <span class="string">'color'</span>, clr);
  set (hz2(2:3), <span class="string">'color'</span>, clr * 0.8);
  xlabel(<span class="string">'Time [d]'</span>)
  ylabel(<span class="string">'Measurement [mV]'</span>)
  legend(h, {<span class="string">'Run 1'</span>,<span class="string">'Run 2'</span>})
  axis tight
  hold off</pre>
<img src="s_gpmodel-5.png" alt="s_gpmodel-5.png">
<footer>
<hr>
<a href="https://www.octave.org">Published with GNU Octave 4.4.0</a>
</footer>
<!--
##### SOURCE BEGIN #####
## GP model of sensor measurements
#
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-07-25
##

## Dependencies
# Load all dependencies of this script
#
pkg load signal      # for autocorrelation
pkg load statistics  # for sampling multivariate normal distribution

##
# To install _gpml_ in Octave 4.4.0 or later run
#
# _pkg install https://bitbucket.org/KaKiLa/gpml/downloads/gpml-4.2.0.tar.gz_
#
pkg load gpml       # for gp models

## Data and model description
# The next plots show the raw data and a simple linear regression
# The results of the linear regression will be used to initialize the GP model
# in the subsequent sections.
#
data = dlmread ('data1.csv', ';', 1, 1);
t1   = data(:,1);
z1   = data(:,2);
N1   = length (z1);
pz1  = polyfit (t1, z1, 1);
z1_  = polyval(pz1, t1);

data = dlmread ('data2.csv', ';', 1, 1);
t2   = data(:,1);
z2   = data(:,2);
N2   = length (z2);
pz2  = polyfit (t2, z2, 1);
z2_  = polyval(pz2, t2);

##
# Plots of the raw data and the linear fit
#
figure (1), clf
  hold on
  h = plot (t1, z1, '^', t2, z2, 'v');
  plot (t1, z1_, '-', 'color', get (h(1), 'color'))
  plot (t2, z2_, '-', 'color', get (h(2), 'color'))
  axis tight
  hold off
  xlabel('Time [d]')
  ylabel('Measurement [mV]')
  legend(h, {'Run 1','Run 2'})

##
# Plots of the residuals, and their autocorrelation functions.
#
res1 = z1 - z1_;
res2 = z2 - z2_;
[acorr1 l1] = xcorr (res1, 'coeff');
[acorr2 l2] = xcorr (res2, 'coeff');

figure (2), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, '^;Run 1;', t2, res2, 'v;Run 2;');
  ylabel('Residual [mV]')
  axis tight

  # Autocorrelation of residuals
  subplot (2, 1, 2)
  hold on
  stem (l1, acorr1, 'color', get (hres(1), 'color'), 'filled');
  stem (l2, acorr2, 'color', get (hres(2), 'color'), 'filled');
  ylabel('Autocorrelation coeff.')
  xlabel('Lag [d]')
  axis tight
  hold off

## Gaussian process (GP) model
# The data generating model is:
#
# $$ \dot{x} = \xi(t) $$
# $$ \dot{y} = x(t) $$
# $$ z_k  = y(t_k) + \zeta_k$$
# it is defined in $t \in [0,\infty)$ with initial conditions
# $x(0) = x_o, y(0) = y_o $
#
# The random variables have the following distributions
# $$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
# $$ \zeta_k \sim N(0,\sigma^2_{\zeta}) $$
#
# This model has the following corresponding  GP.
#
# The state $x(t)$ is white noise,
# $$x(t) \sim \mathcal{N}(x_0, W_0)$$
# where $W_0$ corresponds to the Wiener process covariance.
# This covariance corresponds to _covW(i=0)_ in the gpml package.
#
# The state $y(t)$ is the 1-time integrated Wiener process,
# $$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
# its covariance $W_1$ corresponds to _covW(i=1)_ in the gpml package.
#
# Finally, the observable $z_k := z(t=t_k)$ is modeled as
# $$ z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$
#

# initial values for hyper-parameters
hyp10 = hyp20 = struct ('cov', [], 'mean', [], 'lik', []);

##
# Linear mean function for the observable
#
meanfunc = {@meanSum, {
                       @meanLinear, @meanConst
                      }
           };
hyp10.mean = pz1;
hyp20.mean = pz2;

##
# The likelihood hyper-parameter is the intensity of the observational noise
# the value is $\log \left(\sigma_\zeta \right)$
#
likfunc = @likGauss;
hyp10.lik = hyp20.lik = 0;

##
# Covariance function for the continuous observable $z(t)$
# The value of the hyper-parameter is the logarithm  of the standard deviation
# of the 1-time integrated Wiener process.
#
covfunc = {@covW, 1};
hyp10.cov = hyp20.cov = 0;
# The following is equivalent, if we fix the likelihood hyper-parameter to -Inf
#covfunc = {@covSum, {
#                     {@covW, 1}, @covNoise
#                     }
#           }
#hyp.cov = [0; 0];

args  = {@infExact, meanfunc, covfunc, likfunc};

##
# Find maximum likelihood parameters
#
# Note: the evaluation is done with _evalc_ because the _minimize_ function
# is too verbose.
if ~exist ('hyp1', 'var')
  printf ('Optimizing for Run 1 ...\n'); fflush (stdout);
  #hyp1  = minimize (hyp10, @gp, -1e3, args{:}, t1, z1);
  st = evalc ('hyp1 = minimize (hyp10, @gp, -1e3, args{:}, t1, z1);');
  printf ('%s', st(end-48:end)); fflush (stdout);
  nlml1 = gp (hyp1, args{:}, t1, z1);
endif

if ~exist ('hyp2', 'var')
  printf ('Optimizing for Run 2 ...\n'); fflush (stdout);
  #hyp2  = minimize (hyp20, @gp, -1e3, args{:}, t2, z2);
  st = evalc ('hyp2 = minimize (hyp20, @gp, -1e3, args{:}, t2, z2);');
  printf ('%s', st(end-48:end)); fflush (stdout);
  [nlml2, ~, post] = gp (hyp2, args{:}, t2, z2);
endif
##
# Generate predictions in an extended interval
#
t         = linspace (0, max([t1(:); t2(:)])*1.25, 100).';
[z1_ dz2] = gp (hyp1, args{:}, t1, z1, t);
dz1_      = 1.96 * sqrt (dz2);
[z2_ dz2] = gp (hyp2, args{:}, t2, z2, t);
dz2_      = 1.96 * sqrt (dz2);

##
# Plot the GP prediction
#
figure (3), clf
  hold on
  hz1 = shadowplot (t, z1_, dz1_);
  hz2 = shadowplot (t, z2_, dz2_);
  h = plot (t1, z1, '^', t2, z2, 'v');
  clr = get (h(1), 'color');
  set (hz1.line.center, 'color', clr);
  set ([hz1.line.top hz1.line.bottom], 'color', min (clr*0.8, 1),
    'linewidth', 1);
  set (hz1.patch, 'facecolor', min (clr*3, 1));
  #set (hz1.patch, 'facecolor', clr, 'facealpha', 0.3); # no alpha in html files
  clr = get (h(2), 'color');
  set (hz2.line.center, 'color', clr);
  set ([hz2.line.top hz2.line.bottom], 'color', min (clr*0.8, 1),
    'linewidth', 1);
  set (hz2.patch, 'facecolor', min (clr*3, 1));
  #set (hz2.patch, 'facecolor', clr, 'facealpha', 0.3); # no alpha in html files
  xlabel('Time [d]')
  ylabel('Measurement [mV]')
  legend(h, {'Run 1','Run 2'})
  axis tight
  hold off

##
# The estimation of the quantities of interest
#
printf ("Run 1:\n")
printf ("    x(0) = %.2f mV/day\n", hyp1.mean(1))
printf ("    y(0) = %.2f mV\n", hyp1.mean(2))
printf ("    std. dev. y(t) = %.2f mV\n", exp (hyp1.cov))
printf ("    std. dev. \zeta(t) = %.2f mV\n", exp (hyp1.lik))
printf ("Run 2:\n")
printf ("    x(0) = %.2f mV/day\n", hyp2.mean(1))
printf ("    y(0) = %.2f mV\n", hyp2.mean(2))
printf ("    std. dev. y(t) = %.2f mV\n", exp (hyp2.cov))
printf ("    std. dev. \zeta(t) = %.2f mV\n", exp (hyp2.lik))
fflush (stdout);

##
# Plots of the residuals, and their autocorrelation functions.
#
res1 = z1 - gp (hyp1, args{:}, t1, z1, t1);
res2 = z2 - gp (hyp2, args{:}, t2, z2, t2);
[acorr1 l1] = xcorr (res1);
[acorr2 l2] = xcorr (res2);

figure (4), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, '^;Run 1;', t2, res2, 'v;Run 2;');
  ylabel('Residual [mV]')
  axis tight

  # Autocorrelation of residuals
  subplot (2, 1, 2)
  hold on
  stem (l1, acorr1, 'color', get (hres(1), 'color'), 'filled');
  stem (l2, acorr2, 'color', get (hres(2), 'color'), 'filled');
  xlabel('Time [d]')
  axis tight
  hold off

##
# Posterior (or conditioned) covariance matrices
#
# Note: can be computed more efficiently from the structure _post_
function K = posteriorcov (covfunc, hyp, obs, tst)
  Koo     = feval (covfunc{:}, hyp.cov, obs);
  Kto     = feval (covfunc{:}, hyp.cov, tst, obs);
  Ktt     = feval (covfunc{:}, hyp.cov, tst);
  Koo_inv = cholinv (Koo + exp (2 * hyp.lik) * eye (length (obs)));
  K       = Ktt - Kto * Koo_inv * Kto.'; # Schur complement
endfunction

K1 = posteriorcov (covfunc, hyp1, t1, t);
K2 = posteriorcov (covfunc, hyp2, t2, t);

# Sample the posteriors distributions
z1_samp = mvnrnd (z1_, K1, 25).';
z2_samp = mvnrnd (z2_, K2, 25).';

##
# Plot posterior mean, 95% CI, and samples from the posterior distribution
#
figure (5), clf
  hold on
  plot (t, [z1_samp z2_samp], '-', 'color', [0.8 0.8 0.8], 'linewidth', 1);
  hz1 = plot (t, z1_ + [0 1 -1] .* dz1_, '-', 'linewidth', 1);
  hz2 = plot (t, z2_ + [0 1 -1] .* dz2_, '-', 'linewidth', 1);
  h = plot (t1, z1, '^', t2, z2, 'v');
  clr = get (h(1), 'color');
  set (hz1(1), 'color', clr);
  set (hz1(2:3), 'color', clr * 0.8);
  set (hz1(2), 'color', get (h(1), 'color'));
  clr = get (h(2), 'color');
  set (hz2(1), 'color', clr);
  set (hz2(2:3), 'color', clr * 0.8);
  xlabel('Time [d]')
  ylabel('Measurement [mV]')
  legend(h, {'Run 1','Run 2'})
  axis tight
  hold off
##### SOURCE END #####
-->
</body>
</html>
