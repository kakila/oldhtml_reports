<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Numerical Taylor expansion using moving windows</title>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']] },
  TeX: { equationNumbers: { autoNumber: 'all' } }
});
</script>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

<style>
body > * {
  max-width: 42em;
}
body {
  font-family: "Roboto Condensed", sans-serif;
  padding-left: 7.5em;
  padding-right: 7.5em;
}
pre, code {
  max-width: 50em;
  font-family: monospace;
}
pre.oct-code {
  border: 1px solid Grey;
  padding: 5px;
}
pre.oct-code-output {
  margin-left: 2em;
}
span.comment {
  color: ForestGreen;
}
span.keyword {
  color: Blue;
}
span.string {
  color: DarkOrchid;
}
footer {
  margin-top: 2em;
  font-size: 80%;
}
a, a:visited {
  color: Blue;
}
h2 {
  font-family: "Roboto Condensed", serif;
  margin-top: 1.5em;
}
h2 a, h2 a:visited {
  color: Black;
}
</style>

</head>
<body>
<h1>Numerical Taylor expansion using moving windows</h1>

<p>We use a moving window to extract the local polynomial of a sampled function.
This script is meant to show a trick to use <code>movfun</code> on functions
that consume several input arguments (herein <code>polyfit</code>).</p>

<p>The results are almost equivalent to applying a Savitzsky-Golay smoothing
filter. <code>movfun</code> is much slower than <code>sgolayfilt</code>, but it can be easily
parallelized.</p>

<h2>Contents</h2>
<ul>
<li><a href="#node1">Sampled data</a></li>
<li><a href="#node2">Taylor expansion using <code>movfun</code></a></li>
<li><a href="#node3">Build local approximation</a></li>
<li><a href="#node4">Smoother</a></li>
</ul>

<pre class="oct-code"><span class="comment"># Copyright (C) 2019 Juan Pablo Carbajal</span>
<span class="comment">#</span>
<span class="comment"># This program is free software; you can redistribute it and/or modify</span>
<span class="comment"># it under the terms of the GNU General Public License as published by</span>
<span class="comment"># the Free Software Foundation; either version 3 of the License, or</span>
<span class="comment"># (at your option) any later version.</span>
<span class="comment">#</span>
<span class="comment"># This program is distributed in the hope that it will be useful,</span>
<span class="comment"># but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<span class="comment"># MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<span class="comment"># GNU General Public License for more details.</span>
<span class="comment">#</span>
<span class="comment"># You should have received a copy of the GNU General Public License</span>
<span class="comment"># along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</span>
<span class="comment">#</span>
<span class="comment"># Author: Juan Pablo Carbajal &lt;ajuanpi+dev@gmail.com&gt;</span>
<span class="comment"># Created: 2019-03-22</span>

pkg load signal
<span class="comment">#pkg load parallel</span>
</pre>
<h2><a id="node1">Sampled data</a></h2>
<p>Here we generate samples of a 1D function using a Fourier basis with a finite
number of frequencies</p>

<pre class="oct-code">n  = 200;
t  = linspace (0, 1, n).<span class="string">';</span>
w  = 2 * pi .* [1 2 3];
nw = length (w);
B  = @(t)[sin(w.*t) cos(w.*t)];                    <span class="comment"># Fourier basis</span>
a  = randn(6, 1); [~,o] = sort(abs (a)); a = a(o); <span class="comment"># random growing coefficients</span>
x  = B (t) * a + 0.1;                              <span class="comment"># signal</span>
xn = x + 0.5 * randn (n, 1);                       <span class="comment"># noisy data</span>

deg  = 4;  <span class="comment"># Degree of the (Taylor) polynomial</span>

<span class="comment"># Compute noiseless signal derivatives</span>
b    = B (t);              <span class="comment"># Fourier basis</span>
swap = [(nw+1):2*nw 1:nw]; <span class="comment"># Reordering of the basis when applying derivative</span>
bsig = zeros (deg, 2*nw);  <span class="comment"># Sign of the basis when applying derivatives</span>
bsig(:,1:nw)     = repmat ((-1).^cumsum(mod((0:deg-1).<span class="string">', 2)), 1, nw);</span>
bsig(:,nw+1:end) = repmat ((-1).^cumsum(mod((1:deg).<span class="string">', 2)), 1, nw);</span>
dx = zeros (n, deg+1);     <span class="comment"># derivatives</span>
<span class="keyword">for</span> i=1:deg
  <span class="keyword">if</span> mod (i,2)
    b_ = b(:, swap);
  <span class="keyword">else</span>
    b_ = b;
  <span class="keyword">endif</span>
  dx(:,i+1) = repmat(w.^i, 1, 2) .* (b_ .* bsig(i,:)) * a;
<span class="keyword">endfor</span>
dx(:,1) = x;

figure (1), clf
  plot (t, x, <span class="string">'o-;signal;'</span>, <span class="string">'markersize'</span>, 4, <span class="string">'markerfacecolor'</span>, <span class="string">'auto'</span>);
  hold on
  plot (t, xn, <span class="string">'o;noisy;'</span>, <span class="string">'markersize'</span>, 6);
  hold off
  axis tight
  title (<span class="string">'Sampled signal'</span>)
  legend (<span class="string">'location'</span>, <span class="string">'northeastoutside'</span>);</pre>
<img src="s_movfunn-1.png" alt="s_movfunn-1.png"><h2><a id="node2">Taylor expansion using <code>movfun</code></a></h2>
<p>Regressing a polynomial of degree <code>deg</code> centered at <code>t0</code> provides an
approximation of the Taylor coefficients.
Here we regress a on a moving window centered at all the given data <code>t</code></p>

<pre class="oct-code"><span class="keyword">function</span> p = movpolyfit (idx, t, x, deg, c)
  <span class="comment"># Polynomial regression on a moving window</span>
  <span class="comment">#</span>
  <span class="comment"># t,x are the given sampled data</span>
  <span class="comment"># deg is the degree of the polynomial to be fitted</span>
  <span class="comment"># idx are indexes into the data selecting the current window.</span>
  <span class="comment"># c is the center of the window. Is left to the caller to compute the center</span>
  <span class="comment"># of the window.</span>
  <span class="comment">#</span>
  <span class="comment"># This function is brittle with respect to the 'Endpoints' option passed to</span>
  <span class="comment"># |movfun|</span>

  [wlen d] = size (idx);
  p = zeros (d, deg+1);
  <span class="keyword">if</span> all (idx == 0)
    <span class="comment"># FIXME</span>
    <span class="comment"># This is a bad behavior of |movfun|, it passes all zeros to check output</span>
    <span class="comment"># dimension. It should use part of input.</span>
    <span class="keyword">return</span>
  <span class="keyword">endif</span>
  <span class="comment"># The regression is centered at the current window center, hence the</span>
  <span class="comment"># coefficient of degree n approximate the n-th derivative, e.g. the</span>
  <span class="comment"># intercept of the polynomial approximates the data itself.</span>
  <span class="comment">#</span>
  <span class="comment"># The index are filtered &gt; 0 to handle boundary conditions. Note that we call</span>
  <span class="comment"># |movfun| with 'Endpoints', -1.</span>

  t0 = t(idx(c,:));
  <span class="keyword">for</span> i=1:d
    jdx    = idx(idx(:,i) &gt; 0, i);
    p(i,:) = polyfit(t(jdx) - t0(i), x(jdx), deg);
  <span class="keyword">endfor</span>

  <span class="comment"># cellfun is not more efficient for small number of data points</span>
  <span class="comment"># but movfun allows us to use parcellfun to distribute the computation</span>

  <span class="comment"># idx is in general a matrix, each column indicating a window of the data</span>
  <span class="comment"># Here we convert to cell because |polyfit| does not maps over columns of</span>
  <span class="comment"># its input arguments.</span>
  <span class="comment">#</span>
<span class="comment">#  IDX = mat2cell (idx, wlen, ones (1, d));</span>
<span class="comment">#  p   = cell2mat (cellfun (@(i)polyfit(t(i(i&gt;0))-t(i(c)), x(i(i&gt;0)), deg), ...</span>
<span class="comment">#    IDX, 'unif', 0).');</span>
<span class="comment">#   p   = cell2mat (parcellfun (nproc, ...</span>
<span class="comment">#      @(i)polyfit(t(i(i&gt;0))-t(i(c)), x(i(i&gt;0)), deg), IDX, ...</span>
<span class="comment">#      'UniformOutput', 0).');</span>
<span class="keyword">endfunction</span>

wlen = bitset (deg + 1, 1, 1); <span class="comment"># next odd window length</span>
cent = ceil (wlen / 2);        <span class="comment"># center of odd window</span>
P    = movfun (@(i)movpolyfit(i,t,x,deg,cent), (1:n).<span class="string">', wlen, '</span>Endpoints<span class="string">', -1);</span></pre>

<p><b>Evaluate on noisy data</b></p>

<p>The window is much larger to smooth out noise.
The higher the noise the larger the window.</p>

<pre class="oct-code">wlen = bitset (deg + 1 + 20, 1, 1); <span class="comment"># next odd window length</span>
cent = ceil (wlen / 2);             <span class="comment"># center of odd window</span>

printf (<span class="string">'** movfun: '</span>);
tic
Pn    = movfun (@(i)movpolyfit(i,t,xn,deg,cent), (1:n).<span class="string">', wlen, '</span>Endpoints<span class="string">', -1);</span>
toc

<span class="comment"># Savitsky-Golay smoothing filter with same parameters</span>
printf (<span class="string">'** sgolayfilt: '</span>);
tic
xn_sg = sgolayfilt (xn, deg, wlen);
toc</pre>

<pre class="oct-code-output">** movfun: Elapsed time is 0.077626 seconds.
** sgolayfilt: Elapsed time is 0.0017941 seconds.
</pre>

<p>Plot Taylor coefficients and show that they coincide with the derivatives
scaled by the factorial coefficient, in the case of no noise</p>

<pre class="oct-code">figure (2), clf
  h = plot (t, P);
  hold on
  hd = plot (t, dx ./ factorial(0:deg), <span class="string">'o'</span>);
  hold off
  axis tight
  <span class="keyword">for</span> i=1:(deg+1)
    set (hd(i), <span class="string">'markersize'</span>, 4, <span class="string">'markerfacecolor'</span>, <span class="string">'auto'</span>);
    set (hd(i), <span class="string">'color'</span>, get (h(deg + 2 - i), <span class="string">'color'</span>));
  <span class="keyword">endfor</span>
  legend(hd, strsplit(num2str(0:deg)), <span class="string">'location'</span>, <span class="string">'northeastoutside'</span>);
  title (<span class="string">'Taylor coeffs (noise free data) and derivatives'</span>)</pre>
<img src="s_movfunn-2.png" alt="s_movfunn-2.png">
<p>The same plot for the noisy data</p>

<pre class="oct-code">figure (3), clf
  h = plot (t, Pn);
  hold on
  hd = plot (t, dx ./ factorial(0:deg), <span class="string">'o'</span>);
  hold off
  [ym yM] = bounds (get (hd(end), <span class="string">'ydata'</span>));
  ylim([ym yM]);

  <span class="keyword">for</span> i=1:(deg+1)
    set (hd(i), <span class="string">'markersize'</span>, 4, <span class="string">'markerfacecolor'</span>, <span class="string">'auto'</span>);
    set (hd(i), <span class="string">'color'</span>, get (h(deg + 2 - i), <span class="string">'color'</span>));
  <span class="keyword">endfor</span>
  legend(hd, strsplit(num2str(0:deg)), <span class="string">'location'</span>, <span class="string">'northeastoutside'</span>);
  title (<span class="string">'Taylor coeffs (noisy data) and derivatives'</span>)</pre>
<img src="s_movfunn-3.png" alt="s_movfunn-3.png"><h2><a id="node3">Build local approximation</a></h2>
<p>We compute the Taylor approximation of the data at a few selected points</p>

<pre class="oct-code">t0     = [0.13 0.47 0.81];              <span class="comment"># desired approximation centers</span>
nt0    = length (t0);
[~,it] = min (abs (t - t0));
t0     = t(it);                         <span class="comment"># closest approximation centers.</span>
                                        <span class="comment"># Use interpolation of P for anywhere.</span>
x_app  = xn_app = zeros (n, nt0);
<span class="keyword">for</span> i = 1:nt0
  dtpow       = (t - t0(i)).^(deg:-1:0);
  x_app(:,i)  = dtpow * P(it(i),:).<span class="string">';</span>
  xn_app(:,i) = dtpow * Pn(it(i),:).<span class="string">';</span>
<span class="keyword">endfor</span></pre>

<p>Plot the noiseless signal and its local approximations</p>

<pre class="oct-code">figure (4), clf
  plot (t, x, <span class="string">'linewidth'</span>, 3);
  hold on
  plot(t, x_app);
  plot (t0, x(it), <span class="string">'xk'</span>, <span class="string">'linewidth'</span>, 3)
  hold off
  ylim([min(x), max(x)]);</pre>
<img src="s_movfunn-4.png" alt="s_movfunn-4.png">
<p>Plot the noisy data and its local approximations</p>

<pre class="oct-code">figure (5), clf
  plot (t, x, <span class="string">'--k'</span>);
  hold on
  plot (t, xn, <span class="string">'o'</span>, <span class="string">'markersize'</span>, 6);
  plot(t, xn_app);
  plot (t0, xn(it), <span class="string">'xk'</span>, <span class="string">'linewidth'</span>, 3)
  hold off
  ylim([min(xn), max(xn)]);</pre>
<img src="s_movfunn-5.png" alt="s_movfunn-5.png"><h2><a id="node4">Smoother</a></h2>
<p>The intercept of the polynomial under noisy conditions smooths the data and
approximates the signal.
The results away from the boundaries coincide with the results of the
Savitzsky-Golay smoothing filter.</p>

<pre class="oct-code">figure (6), clf
  plot (t, x, <span class="string">'o-;signal;'</span>, <span class="string">'markersize'</span>, 4, <span class="string">'markerfacecolor'</span>, <span class="string">'auto'</span>);
  hold on
  plot (t, xn, <span class="string">'o;noisy;'</span>, <span class="string">'markersize'</span>, 6);
  plot (t, Pn(:,end), <span class="string">'-;smoother;'</span>, <span class="string">'linewidth'</span>, 3)
  plot (t, xn_sg, <span class="string">'--;SG smoother;'</span>, <span class="string">'linewidth'</span>, 3)
  hold off
  axis tight
  legend (<span class="string">'location'</span>, <span class="string">'northeastoutside'</span>);</pre>
<img src="s_movfunn-6.png" alt="s_movfunn-6.png">
<footer>
<hr>
<a href="https://www.octave.org">Published with GNU Octave 5.1.1</a>
</footer>
<!--
##### SOURCE BEGIN #####
## Numerical Taylor expansion using moving windows
# We use a moving window to extract the local polynomial of a sampled function.
# This script is meant to show a trick to use |movfun| on functions
# that consume several input arguments (herein |polyfit|).
#
# The results are almost equivalent to applying a Savitzsky-Golay smoothing
# filter. |movfun| is much slower than |sgolayfilt|, but it can be easily
# parallelized.
##

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2019-03-22

pkg load signal
#pkg load parallel

## Sampled data
# Here we generate samples of a 1D function using a Fourier basis with a finite
# number of frequencies
#
n  = 200;
t  = linspace (0, 1, n).';
w  = 2 * pi .* [1 2 3];
nw = length (w);
B  = @(t)[sin(w.*t) cos(w.*t)];                    # Fourier basis
a  = randn(6, 1); [~,o] = sort(abs (a)); a = a(o); # random growing coefficients
x  = B (t) * a + 0.1;                              # signal
xn = x + 0.5 * randn (n, 1);                       # noisy data

deg  = 4;  # Degree of the (Taylor) polynomial

# Compute noiseless signal derivatives
b    = B (t);              # Fourier basis
swap = [(nw+1):2*nw 1:nw]; # Reordering of the basis when applying derivative
bsig = zeros (deg, 2*nw);  # Sign of the basis when applying derivatives
bsig(:,1:nw)     = repmat ((-1).^cumsum(mod((0:deg-1).', 2)), 1, nw);
bsig(:,nw+1:end) = repmat ((-1).^cumsum(mod((1:deg).', 2)), 1, nw);
dx = zeros (n, deg+1);     # derivatives
for i=1:deg
  if mod (i,2)
    b_ = b(:, swap);
  else
    b_ = b;
  endif
  dx(:,i+1) = repmat(w.^i, 1, 2) .* (b_ .* bsig(i,:)) * a;
endfor
dx(:,1) = x;

figure (1), clf
  plot (t, x, 'o-;signal;', 'markersize', 4, 'markerfacecolor', 'auto');
  hold on
  plot (t, xn, 'o;noisy;', 'markersize', 6);
  hold off
  axis tight
  title ('Sampled signal')
  legend ('location', 'northeastoutside');

## Taylor expansion using |movfun|
# Regressing a polynomial of degree |deg| centered at |t0| provides an
# approximation of the Taylor coefficients.
# Here we regress a on a moving window centered at all the given data |t|
#

function p = movpolyfit (idx, t, x, deg, c)
  # Polynomial regression on a moving window
  #
  # t,x are the given sampled data
  # deg is the degree of the polynomial to be fitted
  # idx are indexes into the data selecting the current window.
  # c is the center of the window. Is left to the caller to compute the center
  # of the window.
  #
  # This function is brittle with respect to the 'Endpoints' option passed to
  # |movfun|

  [wlen d] = size (idx);
  p = zeros (d, deg+1);
  if all (idx == 0)
    # FIXME
    # This is a bad behavior of |movfun|, it passes all zeros to check output
    # dimension. It should use part of input.
    return
  endif
  # The regression is centered at the current window center, hence the
  # coefficient of degree n approximate the n-th derivative, e.g. the
  # intercept of the polynomial approximates the data itself.
  #
  # The index are filtered > 0 to handle boundary conditions. Note that we call
  # |movfun| with 'Endpoints', -1.

  t0 = t(idx(c,:));
  for i=1:d
    jdx    = idx(idx(:,i) > 0, i);
    p(i,:) = polyfit(t(jdx) - t0(i), x(jdx), deg);
  endfor

  # cellfun is not more efficient for small number of data points
  # but movfun allows us to use parcellfun to distribute the computation

  # idx is in general a matrix, each column indicating a window of the data
  # Here we convert to cell because |polyfit| does not maps over columns of
  # its input arguments.
  #
#  IDX = mat2cell (idx, wlen, ones (1, d));
#  p   = cell2mat (cellfun (@(i)polyfit(t(i(i>0))-t(i(c)), x(i(i>0)), deg), ...
#    IDX, 'unif', 0).');
#   p   = cell2mat (parcellfun (nproc, ...
#      @(i)polyfit(t(i(i>0))-t(i(c)), x(i(i>0)), deg), IDX, ...
#      'UniformOutput', 0).');
endfunction

wlen = bitset (deg + 1, 1, 1); # next odd window length
cent = ceil (wlen / 2);        # center of odd window
P    = movfun (@(i)movpolyfit(i,t,x,deg,cent), (1:n).', wlen, 'Endpoints', -1);

##
# *Evaluate on noisy data*
#
# The window is much larger to smooth out noise.
# The higher the noise the larger the window.
#
wlen = bitset (deg + 1 + 20, 1, 1); # next odd window length
cent = ceil (wlen / 2);             # center of odd window

printf ('** movfun: ');
tic
Pn    = movfun (@(i)movpolyfit(i,t,xn,deg,cent), (1:n).', wlen, 'Endpoints', -1);
toc

# Savitsky-Golay smoothing filter with same parameters
printf ('** sgolayfilt: ');
tic
xn_sg = sgolayfilt (xn, deg, wlen);
toc

##
# Plot Taylor coefficients and show that they coincide with the derivatives
# scaled by the factorial coefficient, in the case of no noise
#
figure (2), clf
  h = plot (t, P);
  hold on
  hd = plot (t, dx ./ factorial(0:deg), 'o');
  hold off
  axis tight
  for i=1:(deg+1)
    set (hd(i), 'markersize', 4, 'markerfacecolor', 'auto');
    set (hd(i), 'color', get (h(deg + 2 - i), 'color'));
  endfor
  legend(hd, strsplit(num2str(0:deg)), 'location', 'northeastoutside');
  title ('Taylor coeffs (noise free data) and derivatives')

##
# The same plot for the noisy data
#
figure (3), clf
  h = plot (t, Pn);
  hold on
  hd = plot (t, dx ./ factorial(0:deg), 'o');
  hold off
  [ym yM] = bounds (get (hd(end), 'ydata'));
  ylim([ym yM]);

  for i=1:(deg+1)
    set (hd(i), 'markersize', 4, 'markerfacecolor', 'auto');
    set (hd(i), 'color', get (h(deg + 2 - i), 'color'));
  endfor
  legend(hd, strsplit(num2str(0:deg)), 'location', 'northeastoutside');
  title ('Taylor coeffs (noisy data) and derivatives')

## Build local approximation
# We compute the Taylor approximation of the data at a few selected points
#
t0     = [0.13 0.47 0.81];              # desired approximation centers
nt0    = length (t0);
[~,it] = min (abs (t - t0));
t0     = t(it);                         # closest approximation centers.
                                        # Use interpolation of P for anywhere.
x_app  = xn_app = zeros (n, nt0);
for i = 1:nt0
  dtpow       = (t - t0(i)).^(deg:-1:0);
  x_app(:,i)  = dtpow * P(it(i),:).';
  xn_app(:,i) = dtpow * Pn(it(i),:).';
endfor

##
# Plot the noiseless signal and its local approximations
#
figure (4), clf
  plot (t, x, 'linewidth', 3);
  hold on
  plot(t, x_app);
  plot (t0, x(it), 'xk', 'linewidth', 3)
  hold off
  ylim([min(x), max(x)]);

##
# Plot the noisy data and its local approximations
#
figure (5), clf
  plot (t, x, '--k');
  hold on
  plot (t, xn, 'o', 'markersize', 6);
  plot(t, xn_app);
  plot (t0, xn(it), 'xk', 'linewidth', 3)
  hold off
  ylim([min(xn), max(xn)]);

## Smoother
# The intercept of the polynomial under noisy conditions smooths the data and
# approximates the signal.
# The results away from the boundaries coincide with the results of the
# Savitzsky-Golay smoothing filter.
#
figure (6), clf
  plot (t, x, 'o-;signal;', 'markersize', 4, 'markerfacecolor', 'auto');
  hold on
  plot (t, xn, 'o;noisy;', 'markersize', 6);
  plot (t, Pn(:,end), '-;smoother;', 'linewidth', 3)
  plot (t, xn_sg, '--;SG smoother;', 'linewidth', 3)
  hold off
  axis tight
  legend ('location', 'northeastoutside');

##### SOURCE END #####
-->
</body>
</html>
