<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Gaussian process regression inflow from Kampala separated by origin category</title>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']] },
  TeX: { equationNumbers: { autoNumber: 'all' } }
});
</script>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

<style>
body > * {
  max-width: 42em;
}
body {
  font-family: "Roboto Condensed", sans-serif;
  padding-left: 7.5em;
  padding-right: 7.5em;
}
pre, code {
  max-width: 50em;
  font-family: monospace;
}
pre.oct-code {
  border: 1px solid Grey;
  padding: 5px;
}
pre.oct-code-output {
  margin-left: 2em;
}
span.comment {
  color: ForestGreen;
}
span.keyword {
  color: Blue;
}
span.string {
  color: DarkOrchid;
}
footer {
  margin-top: 2em;
  font-size: 80%;
}
a, a:visited {
  color: Blue;
}
h2 {
  font-family: "Roboto Condensed", serif;
  margin-top: 1.5em;
}
h2 a, h2 a:visited {
  color: Black;
}
</style>

</head>
<body>
<h1>Gaussian process regression inflow from Kampala separated by origin category</h1>

<h2>Contents</h2>
<ul>
<li><a href="#node1">Load data</a></li>
<li><a href="#node2">GP regressor</a></li>
<li><a href="#node3">Summary of results</a></li>
<li><a href="#node4">Plot results</a></li>
</ul>

<pre class="oct-code"><span class="comment"># Copyright (C) 2018 Juan Pablo Carbajal</span>
<span class="comment">#</span>
<span class="comment"># This program is free software; you can redistribute it and/or modify</span>
<span class="comment"># it under the terms of the GNU General Public License as published by</span>
<span class="comment"># the Free Software Foundation; either version 3 of the License, or</span>
<span class="comment"># (at your option) any later version.</span>
<span class="comment">#</span>
<span class="comment"># This program is distributed in the hope that it will be useful,</span>
<span class="comment"># but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<span class="comment"># MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<span class="comment"># GNU General Public License for more details.</span>
<span class="comment">#</span>
<span class="comment"># You should have received a copy of the GNU General Public License</span>
<span class="comment"># along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</span>

<span class="comment"># Author: Juan Pablo Carbajal &lt;ajuanpi+dev@gmail.com&gt;</span>
<span class="comment"># Created: 2018-01-09</span>

pkg load gpml</pre>
<h2><a id="node1">Load data</a></h2>
<p>We load continuous site variables to build a model for the inverse of the
emptying period <code>SEmptyW</code>.
We also load the categorical variable container type to segment the dataset.
The latter is then removed from the input variables <code>Xname</code>.
<code>Yname</code> contains the output variable to be predicted.</p>

<p>Since the origin category <code>OrCat</code> can only be a cause of the observed variables
we believe that we do not introduce bias in this way, in any case we are
avoiding confounding.</p>

<pre class="oct-code">Xname = {<span class="string">'NUsers'</span>,<span class="string">'CoVol'</span>, <span class="string">'CoAge'</span>, <span class="string">'TrVol'</span>, <span class="string">'IC'</span>, <span class="string">'CoTyp'</span>};
Yname = <span class="string">'SEmptyW'</span>;
[X Y isXcat Xcat_str] = dataset (Xname, Yname, <span class="string">'Kampala'</span>);
Xcat = X(:, isXcat);
ncat = size (Xcat, 2);
X = X(:,!isXcat);
Xname_cat = Xname(isXcat);
Xname = Xname(!isXcat);

<span class="comment">% Indexes to partition output using the categorical variable</span>
idx_cat = categorypartition (Xcat);
cotyp = Xcat_str.(Xname_cat{1}); <span class="comment">%{'Pit latrine', 'Septic tank'};

% Indexes of variables used for mean function
[~, imean] = ismember ({'NUsers', 'CoVol'}, Xname);</span></pre>
<h2><a id="node2">GP regressor</a></h2>
<p>All the regression is performed on logarithmic transformed variables.
We take the negative logarithm of <code>SEmptyW</code> to get the frequency.
After we are in the space where the regression will take place we normalize
the input variables to put them all in similar scales:</p>

<p>$$ y = -\log(Y) $$
$$ x_i = log (X_i) $$
$$ x_i = \frac{x_i - \tilde{x}_i}{\alpha_{x_i}} $$</p>

<p>The GP structure is defined in the function
<a href="https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/inflowgp.m"><code>inflowgp.m</code></a>,
refer to it to know more details.</p>

<pre class="oct-code"><span class="keyword">if</span> !exist(<span class="string">'HYP'</span>, <span class="string">'var'</span>)
  HYP = ARG = struct();
<span class="keyword">endif</span>

<span class="comment">% Verbosity is true, define the variable verbose in the command line to override</span>
<span class="comment">% Make sure verbose is false when generating a html report with publish</span>
<span class="keyword">if</span> ~exist (<span class="string">'verbose'</span>, <span class="string">'var'</span>)
  verbose = false;
<span class="keyword">endif</span>

<span class="comment">% Loop over origin categories and build a model for each</span>
<span class="keyword">for</span> icat = 1:2
  cat_name = cotyp{icat};

  x = log10 (X(idx_cat{icat},:));
  x = x - median (x);
  x = x ./ mean (abs (x));
  y = -log10 (Y(idx_cat{icat})); <span class="comment">% -log Period = log Freq</span>

  <span class="keyword">if</span> !isfield (HYP, cat_name)
    hyp = [];
  <span class="keyword">else</span>
    hyp = HYP.(cat_name);
  <span class="keyword">endif</span>

  <span class="comment">% Choose hyper-parameter constraints for each category</span>
  <span class="comment">% log of the error bounds: 1/7-100 week</span>
  Ferror = sort (log (abs ([-log10(1/7) -log10(100)])));
  <span class="keyword">switch</span> cat_name
    <span class="keyword">case</span> <span class="string">'Pit latrine'</span>
      maxcov = log (0.06); <span class="comment">% Max correction should be about 10% of mean</span>
    <span class="keyword">case</span> <span class="string">'Septic tank'</span>
      maxcov = log (0.122); <span class="comment">% Max correction should be about 10% of mean</span>
  <span class="keyword">endswitch</span>
  [hyp args] = inflowgp (x, y, imean, hyp, Ferror, maxcov, verbose);

  <span class="comment">% Store results for further plotting</span>
  y_data.(cat_name) = y;
  HYP.(cat_name)    = hyp;
  ARG.(cat_name)    = args;
  XX.(cat_name)     = X(idx_cat{icat},:);
  YY.(cat_name)     = 1./Y(idx_cat{icat},:);
<span class="keyword">endfor</span> <span class="comment">% over categories</span>
</pre>
<h2><a id="node3">Summary of results</a></h2>
<p>The coefficient of variation is computed as the ratio between the predictive
standard deviation and the predictive mean.</p>

<p>$$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$</p>

<p>It is used to quantify the amount of correction.</p>

<p>Since for emptying frequency we have a prior model, the correction was
constrained to produce a maximum coefficient of variation of about 10%.</p>

<pre class="oct-code"><span class="keyword">for</span> icat = 1:2
  cat_name = cotyp{icat};
  printf (<span class="string">'\n-- %s --\n'</span>, cat_name);
  report_gp (HYP.(cat_name), ARG.(cat_name), @(x)10.^(x));
<span class="keyword">endfor</span></pre>

<pre class="oct-code-output">
-- Pit latrine --
** Reports of results
Negative log marginal likelihod: 102.94
Mean function parameters
	0.15	0.03	-1.61
Min of inputs
	-2.38	-2.72
Max of inputs
	2.78	2.72
Bounds mean fun: -1.95 -1.18
Covariance amplitude: 0.00
Bounds cov fun: -0.12 0.09
Bounds coeff variation (%): 0.29 10.54
t-distribution: 3.06 0.15
Deviations: 0.26 1.81
Corr coeff: 0.54
-- Septic tank --
** Reports of results
Negative log marginal likelihod: 108.88
Mean function parameters
	0.35	-0.10	-1.93
Min of inputs
	-2.59	-2.11
Max of inputs
	2.59	4.12
Bounds mean fun: -2.69 -1.02
Covariance amplitude: 0.02
Bounds cov fun: -0.17 0.11
Bounds coeff variation (%): 0.13 10.01
t-distribution: 3.04 0.17
Deviations: 0.31 2.05
Corr coeff: 0.58
</pre>
<h2><a id="node4">Plot results</a></h2>
<p>These plots illustrate the performance of the model</p>

<pre class="oct-code">yname = <span class="string">'Frequency [1/week]'</span>;
xname = {<span class="string">'# users'</span>, <span class="string">'Containment V.'</span>, <span class="string">'Containment age'</span>, ...
  <span class="string">'Income level'</span>};
plotresults_gp (1, HYP, ARG, XX, YY, xname, {<span class="string">'log10'</span>, yname}, @(x)10.^(x));
<span class="keyword">for</span> fig =3:4
  h = get(figure(fig), <span class="string">'children'</span>);
  <span class="keyword">for</span> i=1:length(h)
    axes(h(i));
    set (h(i), <span class="string">'yscale'</span>, <span class="string">'log'</span>, <span class="string">'ygrid'</span>, <span class="string">'on'</span>);
    set (h(i), <span class="string">'xscale'</span>, <span class="string">'log'</span>, <span class="string">'xgrid'</span>, <span class="string">'on'</span>);
    axis tight
    xticks ([]); xticks (<span class="string">"auto"</span>); <span class="comment">% force recalculation of ticks</span>
    yticks ([]); yticks (<span class="string">"auto"</span>); <span class="comment">% force recalculation of ticks</span>
    drawnow
  <span class="keyword">endfor</span>
<span class="keyword">endfor</span></pre>
<img src="s_inflow_gp_Kampala-1.png" alt="s_inflow_gp_Kampala-1.png"><img src="s_inflow_gp_Kampala-2.png" alt="s_inflow_gp_Kampala-2.png"><img src="s_inflow_gp_Kampala-3.png" alt="s_inflow_gp_Kampala-3.png"><img src="s_inflow_gp_Kampala-4.png" alt="s_inflow_gp_Kampala-4.png">
<p>This plot shows the relative weight of each variable in the mean function
and the relevance in the covariance function.</p>

<pre class="oct-code">plothypARD (7, HYP.(cotyp{1}), xname, imean);
subplot (2,1,1); title (cotyp{1})
plothypARD (8, HYP.(cotyp{2}), xname, imean);
subplot (2,1,1); title (cotyp{2})</pre>
<img src="s_inflow_gp_Kampala-5.png" alt="s_inflow_gp_Kampala-5.png"><img src="s_inflow_gp_Kampala-6.png" alt="s_inflow_gp_Kampala-6.png">
<footer>
<hr>
<a href="https://www.octave.org">Published with GNU Octave 4.4.0</a>
</footer>
<!--
##### SOURCE BEGIN #####
## Gaussian process regression inflow from Kampala separated by origin category
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

## Load data
# We load continuous site variables to build a model for the inverse of the
# emptying period |SEmptyW|.
# We also load the categorical variable container type to segment the dataset.
# The latter is then removed from the input variables |Xname|.
# |Yname| contains the output variable to be predicted.
#
# Since the origin category |OrCat| can only be a cause of the observed variables
# we believe that we do not introduce bias in this way, in any case we are
# avoiding confounding.
#
Xname = {'NUsers','CoVol', 'CoAge', 'TrVol', 'IC', 'CoTyp'};
Yname = 'SEmptyW';
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
Xcat = X(:, isXcat);
ncat = size (Xcat, 2);
X = X(:,!isXcat);
Xname_cat = Xname(isXcat);
Xname = Xname(!isXcat);

% Indexes to partition output using the categorical variable
idx_cat = categorypartition (Xcat);
cotyp = Xcat_str.(Xname_cat{1}); %{'Pit latrine', 'Septic tank'};

% Indexes of variables used for mean function
[~, imean] = ismember ({'NUsers', 'CoVol'}, Xname);

## GP regressor
# All the regression is performed on logarithmic transformed variables.
# We take the negative logarithm of |SEmptyW| to get the frequency.
# After we are in the space where the regression will take place we normalize
# the input variables to put them all in similar scales:
#
# $$ y = -\log(Y) $$
# $$ x_i = log (X_i) $$
# $$ x_i = \frac{x_i - \tilde{x}_i}{\alpha_{x_i}} $$
#
# The GP structure is defined in the function
# <https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/inflowgp.m |inflowgp.m|>,
# refer to it to know more details.
#
if !exist('HYP', 'var')
  HYP = ARG = struct();
endif

% Verbosity is true, define the variable verbose in the command line to override
% Make sure verbose is false when generating a html report with publish
if ~exist ('verbose', 'var')
  verbose = false;
endif

% Loop over origin categories and build a model for each
for icat = 1:2
  cat_name = cotyp{icat};

  x = log10 (X(idx_cat{icat},:));
  x = x - median (x);
  x = x ./ mean (abs (x));
  y = -log10 (Y(idx_cat{icat})); % -log Period = log Freq

  if !isfield (HYP, cat_name)
    hyp = [];
  else
    hyp = HYP.(cat_name);
  endif

  % Choose hyper-parameter constraints for each category
  % log of the error bounds: 1/7-100 week
  Ferror = sort (log (abs ([-log10(1/7) -log10(100)])));
  switch cat_name
    case 'Pit latrine'
      maxcov = log (0.06); % Max correction should be about 10% of mean
    case 'Septic tank'
      maxcov = log (0.122); % Max correction should be about 10% of mean
  endswitch
  [hyp args] = inflowgp (x, y, imean, hyp, Ferror, maxcov, verbose);

  % Store results for further plotting
  y_data.(cat_name) = y;
  HYP.(cat_name)    = hyp;
  ARG.(cat_name)    = args;
  XX.(cat_name)     = X(idx_cat{icat},:);
  YY.(cat_name)     = 1./Y(idx_cat{icat},:);
endfor % over categories

## Summary of results
# The coefficient of variation is computed as the ratio between the predictive
# standard deviation and the predictive mean.
#
# $$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$
#
# It is used to quantify the amount of correction.
#
# Since for emptying frequency we have a prior model, the correction was
# constrained to produce a maximum coefficient of variation of about 10%.
#
for icat = 1:2
  cat_name = cotyp{icat};
  printf ('\n-- %s --\n', cat_name);
  report_gp (HYP.(cat_name), ARG.(cat_name), @(x)10.^(x));
endfor

## Plot results
# These plots illustrate the performance of the model
#
yname = 'Frequency [1/week]';
xname = {'# users', 'Containment V.', 'Containment age', ...
  'Income level'};
plotresults_gp (1, HYP, ARG, XX, YY, xname, {'log10', yname}, @(x)10.^(x));
for fig =3:4
  h = get(figure(fig), 'children');
  for i=1:length(h)
    axes(h(i));
    set (h(i), 'yscale', 'log', 'ygrid', 'on');
    set (h(i), 'xscale', 'log', 'xgrid', 'on');
    axis tight
    xticks ([]); xticks ("auto"); % force recalculation of ticks
    yticks ([]); yticks ("auto"); % force recalculation of ticks
    drawnow
  endfor
endfor

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
plothypARD (7, HYP.(cotyp{1}), xname, imean);
subplot (2,1,1); title (cotyp{1})
plothypARD (8, HYP.(cotyp{2}), xname, imean);
subplot (2,1,1); title (cotyp{2})
##### SOURCE END #####
-->
</body>
</html>
