<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Gaussian process regression inflow from households</title>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']] },
  TeX: { equationNumbers: { autoNumber: 'all' } }
});
</script>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

<style>
body > * {
  max-width: 42em;
}
body {
  font-family: "Roboto Condensed", sans-serif;
  padding-left: 7.5em;
  padding-right: 7.5em;
}
pre, code {
  max-width: 50em;
  font-family: monospace;
}
pre.oct-code {
  border: 1px solid Grey;
  padding: 5px;
}
pre.oct-code-output {
  margin-left: 2em;
}
span.comment {
  color: ForestGreen;
}
span.keyword {
  color: Blue;
}
span.string {
  color: DarkOrchid;
}
footer {
  margin-top: 2em;
  font-size: 80%;
}
a, a:visited {
  color: Blue;
}
h2 {
  font-family: "Roboto Condensed", serif;
  margin-top: 1.5em;
}
h2 a, h2 a:visited {
  color: Black;
}
</style>

</head>
<body>
<h1>Gaussian process regression inflow from households</h1>

<h2>Contents</h2>
<ul>
<li><a href="#node1">Dependencies</a></li>
<li><a href="#node2">Load data</a></li>
<li><a href="#node3">GP regressor</a></li>
<li><a href="#node4">Summary of results</a></li>
<li><a href="#node5">Plot results</a></li>
<li><a href="#node6">Plot data from both cities</a></li>
</ul>

<pre class="oct-code"><span class="comment"># Copyright (C) 2018 Juan Pablo Carbajal</span>
<span class="comment">#</span>
<span class="comment"># This program is free software; you can redistribute it and/or modify</span>
<span class="comment"># it under the terms of the GNU General Public License as published by</span>
<span class="comment"># the Free Software Foundation; either version 3 of the License, or</span>
<span class="comment"># (at your option) any later version.</span>
<span class="comment">#</span>
<span class="comment"># This program is distributed in the hope that it will be useful,</span>
<span class="comment"># but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<span class="comment"># MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<span class="comment"># GNU General Public License for more details.</span>
<span class="comment">#</span>
<span class="comment"># You should have received a copy of the GNU General Public License</span>
<span class="comment"># along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</span>

<span class="comment"># Author: Juan Pablo Carbajal &lt;ajuanpi+dev@gmail.com&gt;</span>
<span class="comment"># Created: 2018-01-09</span>
</pre>
<h2><a id="node1">Dependencies</a></h2>
<pre class="oct-code">pkg load gpml</pre>
<h2><a id="node2">Load data</a></h2>
<p>We load continuous site variables to build a model for the inverse of the
emptying period for the households in both, Hanoi and Kampala.
<code>Xname</code> contains the input variables.
<code>Yname</code> contains the output variable to be predicted.</p>

<p>The model uses the input variables present in both cities.
<code>CoAge</code> is ignored, otherwise we will have to drop most of Hanoi data,
because for many entries <code>CoAge</code> is equal to <code>SludgeAge</code>.</p>

<pre class="oct-code">Xname = {<span class="string">'NUsers'</span>, <span class="string">'CoVol'</span>, <span class="string">'TrVol'</span>, <span class="string">'OrCat'</span>};
Yname = {<span class="string">'SEmptyW'</span>, <span class="string">'SludgeAge'</span>};
City  = {<span class="string">'Kampala'</span>, <span class="string">'Hanoi'</span>};

<span class="comment">% Loop over cities</span>
<span class="keyword">for</span> c=1:2
  [tmpX tmpY isXcat Xcat_str] = dataset (Xname, Yname{c}, City{c});
  Xcat    = tmpX(:, isXcat);
  idx_cat = categorypartition (Xcat);

  <span class="comment"># Select only Households</span>
  [tf,i] = ismember ({<span class="string">'Household'</span>, <span class="string">'Multiple Household'</span>}, Xcat_str.OrCat);
  <span class="keyword">if</span> length(i(tf)) &gt; 1
    idx_household = cat (idx_cat(i(tf)){:});
  <span class="keyword">else</span>
    idx_household = idx_cat{i(tf)};
  <span class="keyword">endif</span>

  X{c} = tmpX(idx_household, !isXcat);
  Y{c} = tmpY(idx_household);
<span class="keyword">endfor</span>
Xname = Xname(!isXcat);
Y{2} = Y{2} *  52.1429; <span class="comment"># convert years to weeks in Hanoi data</span>
</pre>

<p>Merge the data from both cities</p>

<pre class="oct-code">Yhousehold = cell2mat (Y.<span class="string">');</span>
Xhousehold = cell2mat (X.<span class="string">');</span></pre>
<h2><a id="node3">GP regressor</a></h2>
<p>All the regression is performed on logarithmic transformed variables.
We take the negative logarithm of the emptying period to get the frequency.</p>

<p>After we are in the space where the regression will take place we normalize
the input variables to put them all in similar scales:</p>

<p>$$ y = -\log(Y) $$
$$ x_i = log (X_i) $$
$$ x_i = \frac{x_i - \tilde{x}_i}{\alpha_{x_i}} $$</p>

<pre class="oct-code">y  = -log10 (Yhousehold); <span class="comment">% -log Period = log Freq</span>
x  = log10 (Xhousehold);
x  = x - median (x);
x  = x ./ mean (abs (x));

assert (all(isfinite(x)))
assert (all(isfinite(y)))

[~, imean] = ismember ({<span class="string">'NUsers'</span>, <span class="string">'CoVol'</span>}, Xname);

<span class="keyword">if</span> !exist (<span class="string">'hyp'</span>, <span class="string">'var'</span>)
  hyp = [];
<span class="keyword">endif</span>

<span class="comment">% Verbosity is true, define the variable verbose in the command line to override</span>
<span class="comment">% Make sure verbose is false when generating a html report with publish</span>
<span class="keyword">if</span> ~exist (<span class="string">'verbose'</span>, <span class="string">'var'</span>)
  verbose = false;
<span class="keyword">endif</span>

<span class="comment">% log of the error bounds: 1/7-100 week</span>
Ferror = sort (log (abs ([-log10(1/7) -log10(100)])));
maxcov = log (0.085);               <span class="comment">% Max correction should be about 10% of mean</span>
</pre>

<p>The GP structure is defined in the function
<a href="https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/inflowgp.m"><code>inflowgp.m</code></a>,
refer to it to know more details.</p>

<pre class="oct-code">[hyp args] = inflowgp (x, y, imean, hyp, Ferror, maxcov, verbose);</pre>
<h2><a id="node4">Summary of results</a></h2>
<p>The coefficient of variation is computed as the ratio between the predictive
standard deviation and the predictive mean.</p>

<p>$$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$</p>

<p>It is used to quantify the amount of correction.</p>

<p>Since for emptying frequency we have a prior model, the correction was
constrained to produce a maximum coefficient of variation of about 10%.</p>

<pre class="oct-code">report_gp (hyp, args, @(x)10.^x);</pre>

<pre class="oct-code-output">** Reports of results
Negative log marginal likelihod: 198.08
Mean function parameters
	0.38	0.07	-2.02
Min of inputs
	-2.00	-1.95
Max of inputs
	2.86	4.02
Bounds mean fun: -2.90 -0.96
Covariance amplitude: 0.01
Bounds cov fun: -0.24 0.11
Bounds coeff variation (%): 0.02 10.20
t-distribution: 3.17 0.24
Deviations: 0.41 2.57
Corr coeff: 0.80
</pre>
<h2><a id="node5">Plot results</a></h2>
<p>These plots illustrate the performance of the model</p>

<pre class="oct-code">yname = <span class="string">'Frequency [1/week]'</span>;
xname = {<span class="string">'# users'</span>, <span class="string">'Containment V.'</span>, <span class="string">'Truck V.'</span>};
plotresults_gp (2, hyp, args, Xhousehold, 1./Yhousehold, xname, ...
    {<span class="string">'log10'</span>, yname}, @(x)10.^(x));
h = get(figure(4), <span class="string">'children'</span>);
<span class="keyword">for</span> i=1:length(h)
  axes(h(i));
  set (h(i), <span class="string">'yscale'</span>, <span class="string">'log'</span>, <span class="string">'ygrid'</span>, <span class="string">'on'</span>);
  set (h(i), <span class="string">'xscale'</span>, <span class="string">'log'</span>, <span class="string">'xgrid'</span>, <span class="string">'on'</span>);
  axis tight
  xticks ([]); xticks (<span class="string">"auto"</span>); <span class="comment">% force recalculation of ticks</span>
  yticks ([]); yticks (<span class="string">"auto"</span>); <span class="comment">% force recalculation of ticks</span>
  drawnow
<span class="keyword">endfor</span></pre>

<pre class="oct-code-output">warning: Non-positive limit for logarithmic axis ignored
</pre>
<img src="s_inflow_gp_Household-1.png" alt="s_inflow_gp_Household-1.png"><img src="s_inflow_gp_Household-2.png" alt="s_inflow_gp_Household-2.png"><img src="s_inflow_gp_Household-3.png" alt="s_inflow_gp_Household-3.png">
<p>This plot shows the relative weight of each variable in the mean function
and the relevance in the covariance function.</p>

<pre class="oct-code">plothypARD (6, hyp, xname, imean);</pre>
<img src="s_inflow_gp_Household-4.png" alt="s_inflow_gp_Household-4.png"><h2><a id="node6">Plot data from both cities</a></h2>
<p>Plot emptying frequency vs. each variable in the mean function independently</p>

<pre class="oct-code">figure (5), clf
  np = 15;
  sp = {3:2:2*np, 4:2:2*np};
  <span class="keyword">for</span> i=1:2
    subplot(np,2,sp{i})
    h1 = loglog (X{1}(:,i), 1./Y{1}, <span class="string">'o'</span>,<span class="string">'markerfacecolor'</span>,<span class="string">'auto'</span>);
    hold on
    h2 = loglog (X{2}(:,i), 1./Y{2}, <span class="string">'x'</span>);
    axis tight
    grid on
    xlabel (xname{i})
    <span class="keyword">if</span> i == 1
      ylabel (yname);
    <span class="keyword">endif</span>
    hold off
  <span class="keyword">endfor</span>
  subplot(np,2,[1 2])
  title(<span class="string">'Households'</span>);
  axis off
  legend([h1,h2], City,<span class="string">'Location'</span>,<span class="string">'North'</span>,<span class="string">'Orientation'</span>,<span class="string">'Horizontal'</span>);</pre>
<img src="s_inflow_gp_Household-5.png" alt="s_inflow_gp_Household-5.png">
<footer>
<hr>
<a href="https://www.octave.org">Published with GNU Octave 4.4.0</a>
</footer>
<!--
##### SOURCE BEGIN #####
## Gaussian process regression inflow from households
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Dependencies
#
pkg load gpml

## Load data
# We load continuous site variables to build a model for the inverse of the
# emptying period for the households in both, Hanoi and Kampala.
# |Xname| contains the input variables.
# |Yname| contains the output variable to be predicted.
#
# The model uses the input variables present in both cities.
# |CoAge| is ignored, otherwise we will have to drop most of Hanoi data,
# because for many entries |CoAge| is equal to |SludgeAge|.
#
Xname = {'NUsers', 'CoVol', 'TrVol', 'OrCat'};
Yname = {'SEmptyW', 'SludgeAge'};
City  = {'Kampala', 'Hanoi'};

% Loop over cities
for c=1:2
  [tmpX tmpY isXcat Xcat_str] = dataset (Xname, Yname{c}, City{c});
  Xcat    = tmpX(:, isXcat);
  idx_cat = categorypartition (Xcat);

  # Select only Households
  [tf,i] = ismember ({'Household', 'Multiple Household'}, Xcat_str.OrCat);
  if length(i(tf)) > 1
    idx_household = cat (idx_cat(i(tf)){:});
  else
    idx_household = idx_cat{i(tf)};
  endif

  X{c} = tmpX(idx_household, !isXcat);
  Y{c} = tmpY(idx_household);
endfor
Xname = Xname(!isXcat);
Y{2} = Y{2} *  52.1429; # convert years to weeks in Hanoi data

##
# Merge the data from both cities
Yhousehold = cell2mat (Y.');
Xhousehold = cell2mat (X.');

## GP regressor
# All the regression is performed on logarithmic transformed variables.
# We take the negative logarithm of the emptying period to get the frequency.
#
# After we are in the space where the regression will take place we normalize
# the input variables to put them all in similar scales:
#
# $$ y = -\log(Y) $$
# $$ x_i = log (X_i) $$
# $$ x_i = \frac{x_i - \tilde{x}_i}{\alpha_{x_i}} $$
#
y  = -log10 (Yhousehold); % -log Period = log Freq
x  = log10 (Xhousehold);
x  = x - median (x);
x  = x ./ mean (abs (x));

assert (all(isfinite(x)))
assert (all(isfinite(y)))

[~, imean] = ismember ({'NUsers', 'CoVol'}, Xname);

if !exist ('hyp', 'var')
  hyp = [];
endif

% Verbosity is true, define the variable verbose in the command line to override
% Make sure verbose is false when generating a html report with publish
if ~exist ('verbose', 'var')
  verbose = false;
endif

% log of the error bounds: 1/7-100 week
Ferror = sort (log (abs ([-log10(1/7) -log10(100)])));
maxcov = log (0.085);               % Max correction should be about 10% of mean

##
# The GP structure is defined in the function
# <https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/inflowgp.m |inflowgp.m|>,
# refer to it to know more details.
#
[hyp args] = inflowgp (x, y, imean, hyp, Ferror, maxcov, verbose);

## Summary of results
# The coefficient of variation is computed as the ratio between the predictive
# standard deviation and the predictive mean.
#
# $$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$
#
# It is used to quantify the amount of correction.
#
# Since for emptying frequency we have a prior model, the correction was
# constrained to produce a maximum coefficient of variation of about 10%.
#
report_gp (hyp, args, @(x)10.^x);

## Plot results
# These plots illustrate the performance of the model
#
yname = 'Frequency [1/week]';
xname = {'# users', 'Containment V.', 'Truck V.'};
plotresults_gp (2, hyp, args, Xhousehold, 1./Yhousehold, xname, ...
    {'log10', yname}, @(x)10.^(x));
h = get(figure(4), 'children');
for i=1:length(h)
  axes(h(i));
  set (h(i), 'yscale', 'log', 'ygrid', 'on');
  set (h(i), 'xscale', 'log', 'xgrid', 'on');
  axis tight
  xticks ([]); xticks ("auto"); % force recalculation of ticks
  yticks ([]); yticks ("auto"); % force recalculation of ticks
  drawnow
endfor

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
plothypARD (6, hyp, xname, imean);

## Plot data from both cities
# Plot emptying frequency vs. each variable in the mean function independently
#
figure (5), clf
  np = 15;
  sp = {3:2:2*np, 4:2:2*np};
  for i=1:2
    subplot(np,2,sp{i})
    h1 = loglog (X{1}(:,i), 1./Y{1}, 'o','markerfacecolor','auto');
    hold on
    h2 = loglog (X{2}(:,i), 1./Y{2}, 'x');
    axis tight
    grid on
    xlabel (xname{i})
    if i == 1
      ylabel (yname);
    endif
    hold off
  endfor
  subplot(np,2,[1 2])
  title('Households');
  axis off
  legend([h1,h2], City,'Location','North','Orientation','Horizontal');
##### SOURCE END #####
-->
</body>
</html>
